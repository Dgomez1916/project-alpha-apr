from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            tasks = form.save()
            tasks.author = request.user
            tasks.save()
        return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }

    return render(request, "tasks/create.html", context)


def task_list(request):
    tasks = Task.objects.all()
    context = {
        "task_list": tasks,
    }
    return render(request, "tasks/mine.html", context)
